import homeAction from './homeAction';

const actions = {
  ...homeAction,
};

export default actions;
