function home(
  state = {
    data: [],
  },
  action,
) {
  switch (action.type) {
    case 'REQUEST_HOME':
      return {
        ...state,
        data: action.data,
      };

    default:
      return state;
  }
}

export default home;
