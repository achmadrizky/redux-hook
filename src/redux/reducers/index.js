import {combineReducers} from 'redux';
import homeReducer from './homeReducers';

const reducer = combineReducers({
  home: homeReducer,
});

export default reducer;
