import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';

const App = (props) => {
  const dispatch = useDispatch();
  const home = useSelector((state) => ({
    home: state.home,
  }));

  const submit = () => {
    console.log('home new', home.home);
    dispatch({
      type: 'REQUEST_HOME',
      data: [
        {
          id: 1,
          name: 'Achmad Rizky',
          Kelas: '7A',
        },
        {
          id: 2,
          name: 'Ratih Medikawaty',
          Kelas: '8A',
        },
        {
          id: 3,
          name: 'Jauja Anak Saya',
          Kelas: '9A',
        },
      ],
    });
  };

  return (
    <View>
      <TouchableOpacity onPress={submit}>
        <Text style={{textAlign: 'center', marginTop: 100}}>Button Submit</Text>
      </TouchableOpacity>
    </View>
  );
};

export default App;
